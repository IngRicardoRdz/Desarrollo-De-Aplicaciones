package MySql;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.*;
import javax.swing.JTextField;

import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Conexion {

    int x = 0, y = 0;

    Statement st;
    Connection cn = null;
    String Guardar;

    public Conexion() {

        String ruta;
        String BD;
        String usu;
        String pass;

        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader("C:\\Saves\\save.json"));

            JSONObject jsonObject = (JSONObject) obj;

            ruta = (String) jsonObject.get("url");
            System.out.println(ruta);

            BD = (String) jsonObject.get("bd");
            System.out.println(BD);

            usu = (String) jsonObject.get("usu");
            System.out.println(usu);

            pass = (String) jsonObject.get("pass");
            System.out.println(pass);

            try {
                cn = DriverManager.getConnection(ruta + BD, usu, pass);
                st = cn.createStatement();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error " + ex);
            }

        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Error " + x);
        } catch (IOException | ParseException e) {
            JOptionPane.showMessageDialog(null, "Error " + x);
        }
    }

    public String SaveJson(JTextField TF_Usu, JTextField TF_Password, JTextField TF_BD, JTextField TF_Ruta) {

        JSONObject obj = new JSONObject();

        String URL = "jdbc:mysql://" + TF_Ruta.getText() + "/";

        obj.put("url", URL);
        obj.put("bd", TF_BD.getText());
        obj.put("usu", TF_Usu.getText());
        obj.put("pass", TF_Password.getText());

        try {

            FileWriter file = new FileWriter("C:\\Saves\\save.json");
            file.write(obj.toJSONString());
            file.flush();
            file.close();

            Guardar = "Se Guardo Correctamente";

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error " + e);
        }

        System.out.print(obj);
        return Guardar;
    }

    public ResultSet consulta(JTextField TF_Query) {

        //Connection cn = conexion();
        ResultSet rs = null;

        try {
            st = cn.createStatement();
            rs = st.executeQuery("SELECT * FROM " + TF_Query.getText());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error " + ex);
        }
        return rs;
    }

    public int ins(JTextField TF_Nombre, JTextField TF_Tel) {
        //Connection cn = conexion();

        try {
            String query = "INSERT INTO registros VALUES (0, '" + TF_Nombre.getText() + "', '" + TF_Tel.getText() + "');";
            st.executeUpdate(query);
            x = 1;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error " + ex);
        }
        return x;
    }

    public int deleID(JTextField TF_Valid) {

        //Connection cn = conexion();
        try {
            String sql = "delete "
                    + "from registros "
                    + "where id = " + TF_Valid.getText()+";";
            st.executeUpdate(sql);
            x = 1;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error " + ex);
        }
        return x;
    }

    public int deleNom(JTextField TF_Valid) {

        //Connection cn = conexion();
        try {
            String sql = "delete "
                    + "from registros "
                    + "where nombre like '%" + TF_Valid.getText() + "'";
            st.executeUpdate(sql);
            x = 2;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error " + ex);
        }
        return x;
    }

    public int updaNombre(JTextField TF_Act, JTextField TF_ID) {
        //Connection cn = conexion();
        try {
            String sql = "update registros set nombre = ? where id = ?";
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, TF_Act.getText());
            pst.setString(2, TF_ID.getText());
            pst.execute();
            y = 1;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error " + ex);
        }
        return y;
    }

    public int updaTelefono(JTextField TF_Act, JTextField TF_ID) {
        //Connection cn = conexion();
        try {
            String sql = "update registros set telefono = ? where id = ?";
            PreparedStatement pst = cn.prepareStatement(sql);
            pst.setString(1, TF_Act.getText());
            pst.setString(2, TF_ID.getText());
            pst.execute();
            y = 2;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error " + ex);
        }
        return y;
    }

}