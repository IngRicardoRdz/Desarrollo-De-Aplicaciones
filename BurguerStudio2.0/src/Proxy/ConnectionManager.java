package Proxy;

import java.io.IOException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class ConnectionManager {

    private static boolean hayConexion;

    public static boolean hayConexion() {
        boolean conStatus = false;
        
        try {
            URL u = new URL("https://www.google.com/");
            HttpsURLConnection huc = (HttpsURLConnection) u.openConnection();
            huc.connect();
            conStatus = true;
        } catch (IOException e) {
            conStatus = false;
        }
        
        hayConexion = conStatus;
        return hayConexion;
    }
}