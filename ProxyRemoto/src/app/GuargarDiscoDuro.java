
package app;


import java.util.Date;


public class GuargarDiscoDuro implements IGuardar{

    public static ColaDatos cola = new ColaDatos();
    
    @Override
    public void save(String datosAGuardar) {
        
        System.out.println("Salvando en Disco Duro " + datosAGuardar);
        cola.encolar("Dato " + datosAGuardar + " salvado en " + new Date().toString() );
    }
    
}