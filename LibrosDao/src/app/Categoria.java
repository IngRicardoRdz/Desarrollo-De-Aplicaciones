package app;

import dao.mysql.mysql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author YarelyGms
 */
public class Categoria {

    Connection conMysql;
    Statement st;
    ResultSet rs;

    public Categoria() {
        conMysql = new mysql().getConnection("localhost:3306", "biblioteca", "root", "root");

        try {
            if (conMysql != null) {
                st = conMysql.createStatement();
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public ArrayList<Libro> getLibros(String categoria) {

        ArrayList<Libro> lista = new ArrayList<>();

        try {
            rs = st.executeQuery("select * from `biblioteca`.`libros` where categoria = '" + categoria +"'");

            while (rs.next()) {
                lista.add(new Libro(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return lista;
    }
}
