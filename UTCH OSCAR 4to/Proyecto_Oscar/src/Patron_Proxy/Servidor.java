package Patron_Proxy;

public class Servidor implements Guardar {

    public void save(String datosAGuardar, double precio) { //Se manda al servidor
        
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        
        System.out.println("Ingredientes: " + datosAGuardar);
        System.out.println("Precio total: " + precio);
        System.out.println("Datos enviados a la pizzeria");
    }
}