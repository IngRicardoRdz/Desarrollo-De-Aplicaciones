package controlador;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Auxiliar;
import modelo.ConexionBD;
import modelo.Usuario;

@WebServlet(name = "UsuarioSVT", urlPatterns = {"/UsuarioSVT"})
public class UsuarioSVT extends HttpServlet {

    private ConexionBD conexion;
    private Usuario usuario;
    private Auxiliar auxiliar;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        
        try {
            usuario = new Usuario();
            usuario.setIdUsuario(req.getParameter("txtIdUsuario"));
            usuario.setUsuario(req.getParameter("txtUsuario"));
            usuario.setNombreCompleto(req.getParameter("txtNombre"));
            usuario.setPassWd(req.getParameter("txtPassWd"));
            usuario.setIdArea(1);
            
            conexion = new ConexionBD();
            
            conexion.ejecutarSQL("insert into usuario values(null"
                    + ",'" + usuario.getUsuario() + "'"
                    + ", '" + usuario.getPassWd() + "'"
                    + ", '" + usuario.getNombreCompleto() + "'"
                    + ", " + usuario.getIdArea() + ")"
            );
            
            
            resp.sendRedirect("usuario.jsp");
            
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UsuarioSVT.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioSVT.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UsuarioSVT.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
