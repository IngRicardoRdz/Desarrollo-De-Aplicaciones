package Patron_Proxy;

import java.io.IOException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class Internet { //Se realiza el chequedo de la conectividad 

    private static boolean hayConexion;

    public static boolean hayConexion() {
        boolean conn = false;
        
        try {
            URL u = new URL("https://www.facebook.com/");
            HttpsURLConnection huc = (HttpsURLConnection) u.openConnection();
            huc.connect();
            conn = true;
        } catch (IOException IOE) {
            conn = false;
        }
        
        hayConexion = conn;
        return hayConexion; //Regresa un falso o verdadero dependiendo si existe o no conectividad
    }
}