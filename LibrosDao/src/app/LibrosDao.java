package app;

import dao.libros.LibroDaoImpl;
import java.util.ArrayList;

/**
 *
 * @author YarelyGms
 */
public class LibrosDao {

    public static void main(String[] args) {
        LibroDaoImpl dao = new LibroDaoImpl();
        Categoria categoria = new Categoria();
        
        // Agregar
       Libro libro = new Libro(0, "11 Minutos", "Paulo Coelho", "Erótico");
        dao.add(libro);
        
        // Actualizar
        Libro libro2 = new Libro(6 ,"Divina Comedia", "Dante Alighieri", "Narrativa poética");
        dao.upDate(libro2);
        
        // BORRAR
        dao.delete(5);
        
        // Trer un Libro
        Libro tmp = dao.getLibro(6);
        if(tmp != null)
            System.out.println("Nombre: " + tmp.nombre + " Autor: " + tmp.autor + " Categoria: " + tmp.categoria);
        
        // Traer todos los libros
        ArrayList<Libro> lista = dao.getLibros();
        for (Libro libr : lista) {
            System.out.println("Nombre: " + libr.nombre + " Autor: " + libr.autor + " Categoria: " + libr.categoria);
        }
        
        // Relacion
       ArrayList<Libro> lista2 = categoria.getLibros("Erótico");
        for (Libro libr : lista2) {
            System.out.println("Nombre: " + libr.nombre + " Autor: " + libr.autor + " Categoria: " + libr.categoria);
        }
    }
    
}
