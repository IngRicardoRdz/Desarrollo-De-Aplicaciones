package Patron_Decorador;

public interface Pizza {
    public String getDescription();
    public double getPrice();
}