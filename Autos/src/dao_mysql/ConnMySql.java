package dao_mysql;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnMySql {

   Connection conn;

    public ConnMySql() {
    
        try{
        
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch(Exception e){
            
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public Connection getConnection(String ip, String db, String u, String p){
    
        try{
        
            return DriverManager.
                    getConnection("jdbc:mysql://" + ip + "/" + db, u, p);
        }
        catch(Exception e){
            
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }
}
