package Class;

import MySql.Conexion;
import java.sql.ResultSet;
import javax.swing.JTextField;

public class Agenda {

    Conexion con = new Conexion();
    String mensage = "";
    int res = 0;

    public String Update(JTextField TF_Act, JTextField TF_ID, String x){

        if ("1".equals(x)) {
            res = con.updaNombre(TF_Act, TF_ID);
        } else {
            if ("2".equals(x)) {
                res = con.updaTelefono(TF_Act, TF_ID);
            }
        }

        if (res == 1) {
            mensage = "Nombre Actualizado";
        }
        if (res == 2) {
            mensage = "Telefono Actualizado";
        }
        return mensage;
    }

    public String Delete(JTextField TF_Valid, String x){

        if ("1".equals(x)) {
            res = con.deleID(TF_Valid);
        } else {
            if ("2".equals(x)) {
                res = con.deleNom(TF_Valid);
            }
        }
        if (res == 1) {
            mensage = "Borrado por ID";
        }
        if (res == 2) {
            mensage = "Borrado por Nombre";
        }

        return mensage;

    }

    public String Register(JTextField TF_Nombre, JTextField TF_Tel){

        res = con.ins(TF_Nombre, TF_Tel);

        if (res == 1) {
            mensage = "Registrado Correctamente";
        } else {
            mensage = "No Registrado";
        }

        return mensage;
    }

    public ResultSet Query(JTextField TF_Query){

        ResultSet x = null;
        x = con.consulta(TF_Query);

        return x;
    }

}