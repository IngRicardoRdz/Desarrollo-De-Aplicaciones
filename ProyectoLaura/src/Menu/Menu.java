package Menu;

import app.Carro;
import dao.CarroDaoImple;

public class Menu extends javax.swing.JFrame {

    CarroDaoImple dao = new CarroDaoImple();

    public Menu() {
        initComponents();

        Btm_Insert.setEnabled(false);
        Btm_Delete.setEnabled(false);
        Btm_Update.setEnabled(false);
        TF_Nom.setEnabled(false);
        TF_Mod.setEnabled(false);
        TF_Mar.setEnabled(false);
        Btm_Up.setEnabled(false);
        TF_ID.setEnabled(false);

        setLocationRelativeTo(null);
    }

    public void Usuario(String user) {
        setTitle(user);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        TF_Nom = new javax.swing.JTextField();
        TF_Mod = new javax.swing.JTextField();
        TF_Mar = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Btm_Insert = new javax.swing.JButton();
        Btm_Delete = new javax.swing.JButton();
        Btm_Update = new javax.swing.JButton();
        RB_Insert = new javax.swing.JRadioButton();
        RB_Delete = new javax.swing.JRadioButton();
        RB_Update = new javax.swing.JRadioButton();
        Btm_Ok = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        TF_ID = new javax.swing.JTextField();
        Btm_Up = new javax.swing.JButton();
        RB_1M = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("Nombre");

        jLabel2.setText("Modelo");

        jLabel3.setText("Marca");

        Btm_Insert.setText("Insert");
        Btm_Insert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_InsertActionPerformed(evt);
            }
        });

        Btm_Delete.setText("Delete");
        Btm_Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_DeleteActionPerformed(evt);
            }
        });

        Btm_Update.setText("Update");
        Btm_Update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_UpdateActionPerformed(evt);
            }
        });

        buttonGroup1.add(RB_Insert);
        RB_Insert.setText("Insert");

        buttonGroup1.add(RB_Delete);
        RB_Delete.setText("Delete");

        buttonGroup1.add(RB_Update);
        RB_Update.setText("Update");

        Btm_Ok.setText("OK");
        Btm_Ok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_OkActionPerformed(evt);
            }
        });

        jLabel4.setText("ID or Model");

        Btm_Up.setText("Up");
        Btm_Up.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_UpActionPerformed(evt);
            }
        });

        buttonGroup1.add(RB_1M);
        RB_1M.setText("1:M");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(RB_Update, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(RB_Delete)
                                    .addComponent(RB_Insert))
                                .addGap(18, 18, 18)
                                .addComponent(Btm_Ok))
                            .addComponent(RB_1M))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Btm_Insert)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Btm_Delete)
                                .addGap(25, 25, 25))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(TF_ID, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(Btm_Up)
                                        .addComponent(jLabel4))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(TF_Nom, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 101, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(TF_Mod, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(14, 14, 14)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(Btm_Update))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(TF_Mar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TF_Nom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TF_Mod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TF_Mar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(RB_Insert)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(RB_Delete)
                            .addComponent(Btm_Ok))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RB_Update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RB_1M))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(TF_ID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Btm_Up)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Btm_Insert)
                    .addComponent(Btm_Update)
                    .addComponent(Btm_Delete))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Btm_OkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_OkActionPerformed

        TF_Nom.setText("");
        TF_Mod.setText("");
        TF_Mar.setText("");
        TF_ID.setText("");

        if (RB_Insert.isSelected()) {
            Btm_Insert.setEnabled(true);
            Btm_Delete.setEnabled(false);
            Btm_Update.setEnabled(false);
            TF_Nom.setEnabled(true);
            TF_Mod.setEnabled(true);
            TF_Mar.setEnabled(true);
            Btm_Up.setEnabled(false);
            TF_ID.setEnabled(false);
        }
        if (RB_Delete.isSelected()) {
            Btm_Insert.setEnabled(false);
            Btm_Delete.setEnabled(true);
            Btm_Update.setEnabled(false);
            TF_Nom.setEnabled(false);
            TF_Mod.setEnabled(false);
            TF_Mar.setEnabled(false);
            Btm_Up.setEnabled(false);
            TF_ID.setEnabled(true);
        }
        if (RB_Update.isSelected()) {
            Btm_Insert.setEnabled(false);
            Btm_Delete.setEnabled(false);
            Btm_Update.setEnabled(false);
            TF_Nom.setEnabled(false);
            TF_Mod.setEnabled(false);
            TF_Mar.setEnabled(false);
            Btm_Up.setEnabled(true);
            TF_ID.setEnabled(true);
        }
        if(RB_1M.isSelected()){
            Btm_Insert.setEnabled(false);
            Btm_Delete.setEnabled(false);
            Btm_Update.setEnabled(false);
            TF_Nom.setEnabled(false);
            TF_Mod.setEnabled(false);
            TF_Mar.setEnabled(false);
            Btm_Up.setEnabled(true);
            TF_ID.setEnabled(true);
        }
    }//GEN-LAST:event_Btm_OkActionPerformed

    private void Btm_InsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_InsertActionPerformed
        Carro a = new Carro("0", TF_Nom.getText(), TF_Mod.getText(), TF_Mar.getText());
        dao.add(a);
    }//GEN-LAST:event_Btm_InsertActionPerformed

    private void Btm_DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_DeleteActionPerformed
        dao.delete(TF_ID.getText());
    }//GEN-LAST:event_Btm_DeleteActionPerformed

    private void Btm_UpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_UpActionPerformed

        if(RB_Update.isSelected()){
        Carro c = dao.get(TF_ID.getText());
        TF_Nom.setText(c.nombre);
        TF_Mod.setText(c.modelo);
        TF_Mar.setText(c.marca);
        
        Btm_Insert.setEnabled(false);
        Btm_Delete.setEnabled(false);
        Btm_Update.setEnabled(true);
        TF_Nom.setEnabled(true);
        TF_Mod.setEnabled(true);
        TF_Mar.setEnabled(true);
        }
        
        if(RB_1M.isSelected()){
            Tabla1M t1m = new Tabla1M();
            t1m.Query(TF_ID.getText());
            t1m.Titulo(TF_ID.getText());
            t1m.show();
        }
    }//GEN-LAST:event_Btm_UpActionPerformed

    private void Btm_UpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_UpdateActionPerformed
        Carro auto2 = new Carro(TF_ID.getText(), TF_Nom.getText(), TF_Mod.getText(), TF_Mar.getText());
        dao.upDate(auto2);
    }//GEN-LAST:event_Btm_UpdateActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btm_Delete;
    private javax.swing.JButton Btm_Insert;
    private javax.swing.JButton Btm_Ok;
    private javax.swing.JButton Btm_Up;
    private javax.swing.JButton Btm_Update;
    private javax.swing.JRadioButton RB_1M;
    private javax.swing.JRadioButton RB_Delete;
    private javax.swing.JRadioButton RB_Insert;
    private javax.swing.JRadioButton RB_Update;
    private javax.swing.JTextField TF_ID;
    private javax.swing.JTextField TF_Mar;
    private javax.swing.JTextField TF_Mod;
    private javax.swing.JTextField TF_Nom;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}