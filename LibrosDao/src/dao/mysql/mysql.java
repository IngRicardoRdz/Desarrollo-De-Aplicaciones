package dao.mysql;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author YarelyGms
 */
public class mysql {

    Connection conn;

    public mysql() {

        try {

            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {

            System.out.println("Error: " + e.getMessage());
        }
    }

    public Connection getConnection(String ip, String db, String u, String p) {

        try {

            return DriverManager.
                    getConnection("jdbc:mysql://" + ip + "/" + db, u, p);
        } catch (Exception e) {

            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }
}
