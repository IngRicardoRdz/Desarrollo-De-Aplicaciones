package app;

public class Carro {

    public String id;
    public String nombre;
    public String modelo;
    public String marca;

    public Carro(String idd, String nom, String mod, String mar) {

        id = idd;
        nombre = nom;
        modelo = mod;
        marca = mar;
    }
}
