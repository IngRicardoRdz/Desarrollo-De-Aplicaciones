package Menu_Internal;

import Clases.Speech;
import MySql.Conexion;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Ricardo
 * @version 02/08/17
 */

public class Actualizaciones_Productos extends javax.swing.JInternalFrame {

    /**
     * Creacion delas variables
     * name, scale, stock, buy y descripcion
     * para su manejo global en la clase de Actualizaciones_Productos
     */
    String name = null;
    String scale = null;
    String stock = null;
    String buy = null;
    String description = null;
/**
 * Constructor por defecto de la clase Actualizaciones_Productos
 */
    public Actualizaciones_Productos() {
        initComponents();
        getContentPane().setBackground(Color.black);
/**
 * Hcer que no se puedan modificar las cajas de texto
 *      TF_Buy
 *      TF_CantStock
 *      TF_ProName
 *      TF_ProdDescr
 *      TF_ProducScale
 */
        TF_Buy.setEnabled(false);
        TF_CantStock.setEnabled(false);
        TF_ProName.setEnabled(false);
        TF_ProdDescr.setEnabled(false);
        TF_ProducScale.setEnabled(false);

    }
/**
 *Metodo Bloqueo para inavilitar las cajas de texto
 *      TF_Buy
 *      TF_CantStock
 *      TF_ProName
 *      TF_ProdDescr
 *      TF_ProducScale
 * y dejar desbloqueadas 
 *      TF_ProCode
 *      BTM_Serach
 */
    public void Bloqueo() {
        TF_Buy.setEnabled(true);
        TF_CantStock.setEnabled(true);
        TF_ProName.setEnabled(true);
        TF_ProdDescr.setEnabled(true);
        TF_ProducScale.setEnabled(true);

        TF_ProCode.setEnabled(false);
        BTM_Search.setEnabled(false);
    }
/**
 *Metodo DesBloqueo para avilitar las cajas de texto
 *      TF_Buy
 *      TF_CantStock
 *      TF_ProName
 *      TF_ProdDescr
 *      TF_ProducScale
 * y dejar bloqueadas 
 *      TF_ProCode
 *      BTM_Serach
 */
    public void DesBloqueo() {
        TF_Buy.setEnabled(false);
        TF_CantStock.setEnabled(false);
        TF_ProName.setEnabled(false);
        TF_ProdDescr.setEnabled(false);
        TF_ProducScale.setEnabled(false);

        TF_ProCode.setEnabled(true);
        BTM_Search.setEnabled(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TF_CantStock = new javax.swing.JTextField();
        TF_Buy = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TF_ProdDescr = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        TF_ProCode = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        TF_ProName = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        TF_ProducScale = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        BTM_Search = new javax.swing.JButton();

        setClosable(true);
        setForeground(new java.awt.Color(59, 194, 58));
        setIconifiable(true);
        setTitle("Update Products");

        jLabel2.setForeground(new java.awt.Color(59, 194, 58));
        jLabel2.setText("Product Code");

        TF_ProdDescr.setColumns(20);
        TF_ProdDescr.setRows(5);
        jScrollPane1.setViewportView(TF_ProdDescr);

        jButton1.setBackground(new java.awt.Color(0, 0, 0));
        jButton1.setForeground(new java.awt.Color(59, 194, 58));
        jButton1.setText("Clean");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setForeground(new java.awt.Color(59, 194, 58));
        jLabel3.setText("Product Name");

        jLabel4.setForeground(new java.awt.Color(59, 194, 58));
        jLabel4.setText("Product Scale");

        jLabel1.setForeground(new java.awt.Color(59, 194, 58));
        jLabel1.setText("Product Description");

        jLabel5.setForeground(new java.awt.Color(59, 194, 58));
        jLabel5.setText("QuantityIn Stock");

        jLabel6.setForeground(new java.awt.Color(59, 194, 58));
        jLabel6.setText("Buy Price");

        jButton2.setBackground(new java.awt.Color(0, 0, 0));
        jButton2.setForeground(new java.awt.Color(59, 194, 58));
        jButton2.setText("Update");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        BTM_Search.setText("Search");
        BTM_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTM_SearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(TF_ProCode)
                                        .addComponent(TF_ProName, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)))
                                .addGap(71, 71, 71)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel5))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(TF_ProducScale, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(94, 94, 94)
                                .addComponent(BTM_Search)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 74, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(TF_CantStock, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(TF_Buy, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(8, 8, 8))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addContainerGap())))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(BTM_Search))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(TF_ProCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(TF_CantStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TF_ProducScale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TF_ProName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TF_Buy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
/**
 * Evento de Boton para limpiar todo lo que contengan las cajas de texto 
 * y desbloquear lo que contenga el metodo DesBloquear
 */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        TF_Buy.setText("");
        TF_CantStock.setText("");
        TF_ProCode.setText("");
        TF_ProName.setText("");
        TF_ProdDescr.setText("");
        TF_ProducScale.setText("");
        DesBloqueo();
    }//GEN-LAST:event_jButton1ActionPerformed
/**
 * Evento del boton Buscar para llenar cada variable ya declarada globalmente y mandarlos a imprimir
 */
    private void BTM_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTM_SearchActionPerformed
        Conexion con = new Conexion();
        ResultSet RS = null;
        RS = con.ConAct(TF_ProCode);

        try {
            while (RS.next()) {
                name = RS.getString("productName");
                scale = RS.getString("productScale");
                stock = RS.getString("quantityInStock");
                buy = RS.getString("buyPrice");
                description = RS.getString("productDescription");
            }
        } catch (SQLException ex) {
            System.out.println("Llenado Con Error: " + ex);
        }

        Imprimir();
    }//GEN-LAST:event_BTM_SearchActionPerformed
/**
 * Evento del boton Update para mandar lo que se aya modificado a la bse de datos y hacer los cambios correspondientes
 */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        /**
         * Cracion de los objetos de las clases Conexion y Specch
         */
        Conexion con = new Conexion();
        Speech spch = new Speech();
        /**
         * Validacion para no aceptar campos vacios
         */
        if (TF_Buy.getText().length()==0) {
            JOptionPane.showMessageDialog(null, "The Buy price field can not be empty");
        } else {
            if (TF_CantStock.getText().length()==0) {
                JOptionPane.showMessageDialog(null, "The Quantity In Stock field can not be empty");
            } else {
                if (TF_ProName.getText().length()==0) {
                    JOptionPane.showMessageDialog(null, "The Product Name field can not be empty");
                } else {
                    if (scale.length()==0) {
                        JOptionPane.showMessageDialog(null, "Scale field may be empty");
                    } else {
                        if (TF_ProdDescr.getText().length()==0) {
                            JOptionPane.showMessageDialog(null, "The Product Description field may be empty");
                        } else {
                            /**
                             * Llevar los datos correspondientes a el metodo Update de la clase Conexion
                             */
                            String res = con.Update(TF_ProCode, TF_Buy, TF_CantStock, TF_ProName, TF_ProdDescr, scale);
                            /**
                            * La varable res es mandado a la clase Speech para reproducir la bienvenida a la aplicacion
                            */
                            spch.Play(res);
                        }
                    }
                }
            }
        }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     *Metodo que hace que se impriman los resultados obtenidos de la consulta hecha a la base de datos
     * y colocarlos en su respectiva caja de texto
     */
    public void Imprimir() {
        TF_Buy.setText(buy);
        TF_CantStock.setText(stock);
        TF_ProName.setText(name);
        TF_ProdDescr.setText(description);
        TF_ProducScale.setText(scale);
        Bloqueo();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTM_Search;
    private javax.swing.JTextField TF_Buy;
    private javax.swing.JTextField TF_CantStock;
    private javax.swing.JTextField TF_ProCode;
    private javax.swing.JTextField TF_ProName;
    private javax.swing.JTextArea TF_ProdDescr;
    private javax.swing.JTextField TF_ProducScale;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
