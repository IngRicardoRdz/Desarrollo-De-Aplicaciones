public class NewSingleton {

    private String url;
    private String baseDatos;
    private static NewSingleton miconfigurador;

    public static NewSingleton getConfigurador(String url, String baseDatos) {

        if (miconfigurador == null) {

            miconfigurador = new NewSingleton(url, baseDatos);
        }
        return miconfigurador;
    }

    private NewSingleton(String url, String baseDatos) {

        this.url = url;
        this.baseDatos = baseDatos;

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBaseDatos() {
        return baseDatos;
    }

    public void setBaseDatos(String baseDatos) {
        this.baseDatos = baseDatos;
    }
}
