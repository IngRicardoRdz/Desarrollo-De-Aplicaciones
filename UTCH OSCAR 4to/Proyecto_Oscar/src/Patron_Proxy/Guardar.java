package Patron_Proxy;

public interface Guardar {
    public void save(String datosAGuardar, double precio);
}