package App;

import MySql.Conexion;
import java.awt.Color;

public class BD extends javax.swing.JFrame {

String valor;
    public BD() {
        initComponents();
        
        Color myColor = Color.decode("#151515");
        
        this.getContentPane().setBackground(myColor);
        jPanel1.setBackground(myColor);
        jPanel2.setBackground(myColor);
        
        jLabel2.setForeground(Color.CYAN);
        jLabel3.setForeground(Color.CYAN);
        jLabel4.setForeground(Color.CYAN);
        jLabel5.setForeground(Color.CYAN);
        jLabel17.setForeground(Color.CYAN);
        
        setLocationRelativeTo(null);
        L_Mensage.setVisible(false);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        Btm_Provar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        TF_BD = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TF_Password = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        TF_Usu = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        TF_Ruta = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        L_Mensage = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new java.awt.GridLayout(9, 2, 0, 15));

        Btm_Provar.setText("Guardar");
        Btm_Provar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Btm_Provar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btm_ProvarActionPerformed(evt);
            }
        });
        jPanel1.add(Btm_Provar);

        jLabel2.setText("Base de Datos");
        jPanel1.add(jLabel2);
        jPanel1.add(TF_BD);

        jLabel3.setText("Password");
        jPanel1.add(jLabel3);
        jPanel1.add(TF_Password);

        jLabel4.setText("Usuario");
        jPanel1.add(jLabel4);
        jPanel1.add(TF_Usu);

        jLabel5.setText("Ruta");
        jPanel1.add(jLabel5);
        jPanel1.add(TF_Ruta);

        jPanel2.setLayout(new java.awt.GridBagLayout());

        L_Mensage.setText("Mensaje");
        jPanel2.add(L_Mensage, new java.awt.GridBagConstraints());

        jLabel17.setText("Regresar");
        jLabel17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 367, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(97, 97, 97)
                .addComponent(jLabel17)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Btm_ProvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btm_ProvarActionPerformed
        Conexion con = new Conexion();
        valor = con.SaveJson(TF_Usu, TF_Password, TF_BD, TF_Ruta);
        
        if(valor != null){
            L_Mensage.setVisible(true);
            L_Mensage.setForeground(Color.green);
            L_Mensage.setText(valor);
        }
    }//GEN-LAST:event_Btm_ProvarActionPerformed

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
        Menu m = new Menu();
        dispose();
        m.show();
    }//GEN-LAST:event_jLabel17MouseClicked


    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new BD().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btm_Provar;
    private javax.swing.JLabel L_Mensage;
    private javax.swing.JTextField TF_BD;
    private javax.swing.JTextField TF_Password;
    private javax.swing.JTextField TF_Ruta;
    private javax.swing.JTextField TF_Usu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
