package Main;

import Patron_Decorador.Mozzarella;
import Patron_Decorador.Pizza;
import Patron_Decorador.PlainPizza;
import Patron_Decorador.TomatoSauce;
import Patron_Proxy.GuardarDatos;

public class Proyecto_Oscar {

    public static void main(String[] args) {

        Pizza pizza = new Mozzarella(new TomatoSauce(new PlainPizza())); //Se realiza la llamada donde se agregan los ingredientes
        GuardarDatos gd = new GuardarDatos(); //Se creo  el objeto para madarlo a guardar
        gd.save(pizza.getDescription(), pizza.getPrice()); // Se manda a guardar datos 

    }

}