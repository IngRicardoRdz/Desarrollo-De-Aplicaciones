
package app;


public class ConnectionManager {

    private static boolean hayConecxion;
    
    public ConnectionManager(){
        
        hayConecxion = false;
    }
    
    public static void conectate(){
        
        hayConecxion = true;
    }
    
    public static void desConectate(){
        
        hayConecxion = false;
    }
    
    public static boolean hayConecxion(){
        
        return hayConecxion;
    }
}