package Decorator;

public class Queso extends DecoradorHamburguesa{
	private Hamburguesa hamburguesa;

	public Queso(Hamburguesa h){
		this.hamburguesa = h;
	}

	public String getDescripcion(){
		return hamburguesa.getDescripcion()+" + Queso";
	}
}