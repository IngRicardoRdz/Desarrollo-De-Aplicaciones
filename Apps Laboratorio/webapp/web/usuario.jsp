<%-- 
    Document   : usuario
    Created on : 24-may-2017, 17:41:50
    Author     : LS12DOCENTE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="js/funciones.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Web App</title>
    
        <script>
            
            function asignarId(v1){
                
                document.getElementById("txtIdUsuario").value = v1[0];
                document.getElementById("txtNombre").value = v1[1];
            }
 
        </script>
    
    
    
    </head>
    
    
    
    
    
    <body onload="cargarUsuarios()">
        <h1>Usuario</h1>
        
        <form name="UsuarioFRM" method="post" action="UsuarioSVT">
            
            <input name="txtIdUsuario" id="txtIdUsuario" type="hidden" value="0"/>
            
            <label>Nombre</label>
            <input name="txtNombre" id="txtNombre" type="text" placeholder="Nombre Completo"/>
            
            <label>Usuario</label>>
            <input name="txtUsuario" id="txtUsuario" type="text" placeholder="Usuario" maxlength="15"/>
            
            <label>Password</label>
            <input name="txtPassWd" id="txtPassWd" type="password" maxlength="15"/>
            
            <label>Área</label>
            <select name="cboArea">
                
                <option>Recursos Humanos</option>
                <option>Contabilidad</option>
                <option>Finanzas</option>
                <option>Sistemas</option>
                <option>Almacén</option>
                
            </select>
            
            <input name="cmd" type="submit" value="Aceptar"/> 
            <input name="cmd" type="submit" value="Modificar"/> 
            <input name="cmd" type="submit" value="Eliminar"/> 
            
        </form>
        
        <div id="tblUsuario">
            
            <!-- Aquì se va a incrustar elarchivo qryusuario-->
        </div>
        
        
    </body>
</html>
