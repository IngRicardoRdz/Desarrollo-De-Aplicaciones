package fabricas;

public class Fabrica {
    
    public static MotorSQL getMotor(String m){
         
        if(m.equals("MySLQ")){
             return (MotorSQL) new MySQL();
         }
         if(m.equals("Oracle")){
             return (MotorSQL) new Oracle();
         } 
        return null;
    }
    
}
